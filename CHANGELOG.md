# Change Log
All notable changes to this project will be documented in this file.

## 2015-04-01	Imtiaz Rahi
### Added
* country-jpa-orm-0.1.0-SNAPSHOT
* JUnit test cases developed using Hibernate
* Country entity defined
