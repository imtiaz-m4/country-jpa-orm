package net.m4.country;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestHibernateORM {
	private static final String CODE = "ZZ";
	private static final String NAME = "Zyborg Country";
	private static SessionFactory factory;
	private Session session;

	@SuppressWarnings("deprecation")
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		factory = new Configuration().configure().addAnnotatedClass(Country.class).buildSessionFactory();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		factory.close();
	}

	@Before
	public void setUp() throws Exception {
		session = factory.openSession();
	}

	@After
	public void tearDown() throws Exception {
		session.close();
	}

	private Country sample() {
		Country ob = new Country(CODE, "Zyborg Country");
		ob.capital = "Zyle Cap";
		ob.area = new BigDecimal(10090.57).setScale(1, BigDecimal.ROUND_UP);
		ob.continent = "Zcont";
		ob.currency = "BDT";
		ob.iso3 = "ZZZ";
		return ob;
	}

	@SuppressWarnings("unchecked")
	@Test
	public void test1Read() {
		List<Country> list = session.createCriteria(Country.class).list();
		assertNotNull(list);
		for (Country it : list) {
			System.out.println(it);
		}
	}

	@Test
	public void test2Insert() {
		Transaction tx = session.beginTransaction();
		Country ob = sample();
		session.save(ob);
		tx.commit();
		System.out.println("created " + ob);
	}

	@Test
	public void test3Find() {
		Country ob = (Country) session.load(Country.class, CODE);
		assertNotNull(ob);
		System.out.println("Found " + ob);
		assertEquals(NAME, ob.getName());
	}

	@Test
	public void test4Update() {
		Transaction tx = null;
		long people = 122243L;
		try {
			tx = session.beginTransaction();
			Country ob = (Country) session.get(Country.class, CODE);
			assertNotNull(ob);
			assertEquals(NAME, ob.getName());
			ob.population = people;
			tx.commit();
			System.out.println("Updated " + ob);
		} catch (HibernateException e) {
			e.printStackTrace();
			if (tx != null) tx.rollback();
		}
		Country ob = (Country) session.load(Country.class, CODE);
		assertNotNull(ob);
		assertEquals(people, ob.population);
	}

	@Test
	public void test5Delete() {
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			Country ob = (Country) session.get(Country.class, CODE);
			assertNotNull(ob);
			session.delete(ob);
			tx.commit();
		} catch (HibernateException e) {
			e.printStackTrace();
			if (tx != null) tx.rollback();
		}
		assertNull(session.get(Country.class, CODE));
	}
}
