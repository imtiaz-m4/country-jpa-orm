package net.m4.country;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateApp {
	private static SessionFactory factory;

	public HibernateApp() {
		factory = getFactory();
	}

	public static void main(String[] args) {
		HibernateApp app = new HibernateApp();
		for (Country it : app.list()) {
			System.out.println(it);
		}
		System.exit(0);
	}

	@SuppressWarnings("deprecation")
	private SessionFactory getFactory() {
		return new Configuration().configure().addAnnotatedClass(Country.class).buildSessionFactory();
	}

	@SuppressWarnings("unchecked")
	private List<Country> list() {
		Session session = factory.openSession();
		List<Country> list = session.createCriteria(Country.class).list();
		session.close();
		return list;
	}
}
