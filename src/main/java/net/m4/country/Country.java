package net.m4.country;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Arrays;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "countries")
@Access(AccessType.FIELD)
public class Country implements Serializable, Comparable<Country> {
	private static final long serialVersionUID = -6156967197347590894L;

	@Id
	String code;
	@Column(nullable = false)
	String name;

	String currency;
	BigDecimal area;
	long population;
	String capital;
	String continent;
	String languages;

	String north, south, east, west;

	@Column(name = "isoalpha3")
	String iso3;
	@Column(name = "isonumeric")
	String isoNo;

	protected Country() {
		super();
	}

	public Country(String code, String name) {
		super();
		this.code = code;
		this.name = name;
	}

	@Override
	public int compareTo(Country o) {
		return code.compareTo(o.code);
	}

	@Transient
	public String[] getLanguages() {
		return languages.split(",");
	}

	@Transient
	public void setLanguages(String[] lang) {
		String str = Arrays.toString(lang);
		this.languages = str.substring(1, str.length() - 1);
	}

	@Override
	public String toString() {
		return "Country [code=" + code + ", name=" + name + ", iso3=" + iso3 + ", isoNo=" + isoNo + ", currency=" + currency + ", area=" + area
				+ ", population=" + population + ", capital=" + capital + ", continent=" + continent
				+ ", north=" + north + ", south=" + south + ", east=" + east + ", west=" + west
				+ (languages != null && !languages.isEmpty() ? ", languages=" + getLanguages()[0] : "")
				+ "]";
	}

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public String getCurrency() {
		return currency;
	}

	public BigDecimal getArea() {
		return area;
	}

	public long getPopulation() {
		return population;
	}

	public String getCapital() {
		return capital;
	}

	public String getContinent() {
		return continent;
	}

	public String getNorth() {
		return north;
	}

	public String getSouth() {
		return south;
	}

	public String getEast() {
		return east;
	}

	public String getWest() {
		return west;
	}

	public String getIso3() {
		return iso3;
	}

	public String getIsoNo() {
		return isoNo;
	}

}
